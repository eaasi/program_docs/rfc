# EaaSI RFCs

This repository is a draft workspace for team members to propose and discuss functional requirements and design of the EaaSI platform.

This is intended to be relatively high-level documentation, to be used as reference for the team to come to consensus on suitable development approach(es) to complex features or system architecture (that will then be broken down into Issues in relative repository trackers and/or tasks on the roadmap as needed). 

In the interest of transparency we are making this repository publicly visible but it is generally not inteded for public comment, and we reserve the option to alter permissions after evaluation. Please use the [EaaSI Community Forum](https://forum.eaasi.cloud) to ask questions or leave feedback relevant to the EaaSI development roadmap.

## Contribution

These instructions are intended as tips/recommendations for EaaSI team members to contribute and comment on RFC documents using this repsitory.

This repository is public on GitLab, so it should be possible for anyone with a GitLab account to submit an RFC via merge request regardless of whether their account is a member of the "eaasi" GitLab group. However, there are one or two tweaks in the specific procedure if the account is NOT a member of the "eaasi" group (involving making a "fork" of the `rfc` repository and suggesting changes from that fork, rather than just being able to make a new branch directly in the `eaasi/program_docs/rfc` repository on GitLab).

This comes into play because adding members to the "eaasi" group or even just this repository individually can affect costs for the GitLab Premium subscription.

Steps marked with a star (*) can be completed either working in the GitLab interface or with desktop text editing and git management tools, to personal taste + preference.

1. Create a new branch of the `eaasi/program_docs/rfc` repository. (Name your branch something short and descriptive to your proposal)*

2. In the new branch, create a new Markdown (.md) file. (Name the file something short but descriptive to your proposal; recommended to use the same name as the branch name)*

3. Author your proposal document in the new .md file. (For now we will not templatize these; structure the doc/proposal as you see fit to the proposal in question)*

    You can find guides to Markdown and GitLab-flavored Markdown syntax here: 

    https://www.markdownguide.org/

    https://docs.gitlab.com/ee/user/markdown.html

4. When finished writing, commit your full proposal to the proposal branch.*

5. In the GitLab web interface for the `rfc` repository, start a new Merge Request. Select your new branch (with the proposal in it) as the "source branch" and the "main" branch of the repository as the "target" branch.

6. Mark the Merge Request as a "draft" (to prevent it from being prematurely merged to the repository before discussion and consensus is reached).

7. Notify other EaaSI team members (in MS Teams) that your RFC is available for review and link to the Merge Request.

8. To review other's proposals, navigate to the linked/relevant Merge Request page, then click on the "Changes" tab.

9. Click the "Comment" speech bubble icon at the top right of the Markdown viewer on the "Changes" tab to leave a general comment or question relevant to the entire proposal.

10. While reading or reviewing individual lines of the proposal document, click the "Comment" speech bubble icon while hovering over that line to leave a comment or question (you can also click and drag to select and comment on multiple lines of the document at once). Note also that the WSYWIG editor that opens for leaving comments has a "Insert Suggestion" button (the small "page" icon right next to the "Preview" button) that allows you to directly suggest edits to the text that can be accepted or rejected by the author of the proposal (much like "Suggesting" mode in Google Docs or Office).

11. If you click the settings button at the top right of the Markdown viewer, you can at any time select "Hide All Comments" to read and review the whole document with comments collapsed, if comments become too numerous/difficult to manage. You can also click on individual lines/comment threads to collapse or expand them.

12. The settings button also has a "View file" option that will open a separate tab with the whole document rendered in rich text. You will not be able to leave comments or questions directly on this rendered version but it may be helpful for actually reading or reviewing the whole doc, and then you can return to the Merge Request to leave specific comments as-needed.

13. The "Changes" tab on the Merge Request should always default to displaying the "latest version" of the document - this means that the author should actually feel free to commit new text or changes to the document at any point (including Accepting edits suggested via the "Insert Suggestion" feature described in #10 above) and not worry about getting out-of-sync with the rest of the team. Existing comment threads persist in everyone's default view of the Merge Request until explicitly resolved.

14. With the team, set a timeline for review and discussion of the proposal. Timelines should include the chance for both asynchronous review (via the comment methods described above) and synchronous discussion (either in a regular dev check-in or dedicated Zoom call).

15. All discussion threads and suggested edits should be resolved before the Merge Request can be completed and the RFC document considered adopted. The author of the RFC should take primary responsibility for closing/resolving any open questions and merging any agreed-upon edits.

16. After discussion, once all comment threads have been resolved and any suggested or new edits have been committed by the original author to the branch, the author should remove the "Draft" label from the Merge Request (by clicking "Mark as Ready" in the Merge Request overview). Notify the team and provide some nominal amount of time for final review between removing the "Draft" label and completing the Merge Request.

17. The source/proposal branch can and should be deleted upon successful merge/adoption of an RFC document.